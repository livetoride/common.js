$(document).ready(function(){
	var markup = document.documentElement.innerHTML;
	var markup2 = $('body').attr('id');
	var errorIdentificator = '<meta name="description" content="Oops, nastala chyba 500">';
	var nonErrorIdentificator = 'admin-oops-default';
	function goToErrorPresenter()
	{
		var append = "/ooops"
		if( window.location.pathname.indexOf("/admin/") > -1 )
		{
			var append = "/admin/ooops";
		}
		window.location.replace( window.location.protocol +"//"+ window.location.host + append );
	};

	if(markup2 != nonErrorIdentificator ) {
		if(markup.indexOf(errorIdentificator) > -1) {
				goToErrorPresenter();
		}
		if(markup.indexOf("The server encountered an internal error and was unable to complete your request") > -1) {
				goToErrorPresenter();
		}

	}

});
