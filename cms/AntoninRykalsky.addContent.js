

(function ( addContent, undefined ) {

	addContent.saveAddWrapper = function( what ) {
		$('.wrapper-block').parent().append('<div class="wrapper-block"></div>');
	};
	addContent.saveAddContent= function( what ) {
		var gridder = AntoninRykalsky.gridder;
		var wrapper = $('.grid-block:last').data('wrapper');
		var siteid = $('.wrapper-block').data('siteid');
		$.nette.ajax({
			url: '?do=addStuff&id='+siteid,
			data: {'wrapper': wrapper, 'what': what }
		}).done(function() {
			AntoninRykalsky.gridrepository.afterInvalidation();
		});
	};

	var displayAddContentPanel = function() {
		var newPanel = $('.add-content-panel-base').clone();
		newPanel.removeClass('add-content-panel-base');
		newPanel.addClass('add-content-panel');
		newPanel.appendTo( '#imContent' );
		displayAddContentPanelResize();
	}

	var displayAddContentPanelResize = function() {
		var pw = $('.add-content-panel').width();
		var ph = $('.add-content-panel').height();
		var cw = $('#imContent').width();
		var ch = $('#imContent').height();

		$('#imContent').css('position', 'relative');
		$('.add-content-panel').css('top', 70 + "px");
		$('.add-content-panel').css('left', (cw-pw)/2 + "px");
		$('.add-content-panel').css('position', 'fixed');
	};


	addContent.register= function() {
		$('.add-content').click(function() {
			// addContent.renderer.panel();
			displayAddContentPanel();

			$('.add-content-panel .close').click(function() {
				$(this).parent('.add-content-panel').hide();
			});
			$('.add-content-panel .add-stuff').click(function(){
				var what = $(this).attr('data-do');
				addContent.saveAddContent(what);
				$(this).parent('.add-content-panel').hide();
				return false;
			});
		});

	};

}) ( AntoninRykalsky.addContent = AntoninRykalsky.addContent || {} );
