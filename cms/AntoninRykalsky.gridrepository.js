
(function ( gridrepository, undefined ) {
	gridrepository.registerDeleteHandler = function(){
		$('.stored-grid .delete-grid').click(function(){
			var really = confirm('Opravdu odstranit z zařazení?');
			if( !really ) {
				return false;
			}

			var href = $(this).attr('data-href');
			$.nette.ajax({
				url: href
			}).done(function() {
				AntoninRykalsky.gridrepository.afterInvalidation();
			});
		});
	}

	/** umožní vložení mezi aktivní gridy */
	var privateDisableSelection = function() {
		$( ".mydrop .stored-grid" ).hover(function() {
			// vypnutí selekce textu při hoveru
			document.onselectstart = function () { return false; };
		  },function() {
			// zapnutí selekce textu při hoveru
			document.onselectstart = null;
		  });
		$( ".mydrop .stored-grid" ).draggable({
			revert: "invalid",
			helper: "clone",
			start:function( event, ui ) {
				// vypnutí selekce textu při startu dragu
				document.onselectstart = function () { return false; };
			},
			stop: function( event, ui ) {
				// vypnutí selekce textu po skončení dragu
				document.onselectstart = null;
			}
		});
	}

	gridrepository.afterStoreToRepo = function() {
		AntoninRykalsky.gridder.register();
		gridrepository.registerDeleteHandler();
		AntoninRykalsky.CmsGridOption.register();
		AntoninRykalsky.CmsGridOption.hideLinks();
		gridrepository.allowGridStoreable();
		AntoninRykalsky.gridrepository.allowGridPaste();

		$(".cms-remove-from-menu").click(function(event){
			var really = confirm('Opravdu odstranit z zařazení?');
			if( !really ) {
				return false;
			}

			var href = $(this).attr('data-href');
			$.nette.ajax({
				url: href
			}).done(function() {
				AntoninRykalsky.gridrepository.afterInvalidation();
			});
		});
	};
	
	gridrepository.afterInvalidation = function() {
		AntoninRykalsky.gridder.register();
		AntoninRykalsky.gridder.registerDeleteButton();
		gridrepository.registerDeleteHandler();
		AntoninRykalsky.CmsGridOption.register();
		AntoninRykalsky.CmsGridOption.hideLinks();
		gridrepository.allowGridStoreable();
		AntoninRykalsky.gridrepository.allowGridPaste();

		$(".cms-remove-from-menu").click(function(event){
			var really = confirm('Opravdu odstranit z zařazení?');
			if( !really ) {
				return false;
			}

			var href = $(this).attr('data-href');
			$.nette.ajax({
				url: href
			}).done(function() {
				AntoninRykalsky.gridrepository.afterInvalidation();
			});
		});
	};

	gridrepository.allowGridPaste = function() {
		privateDisableSelection();
		$('.grid-sortable').droppable({
			activeClass: "mydrop-active",
			hoverClass: "mydrop-hover",
			accept: ".stored-grid",
			drop: function( event, ui ) {
				var idgrid = ui.helper.data('id');
				ui.helper.remove();
				$.nette.ajax({
					data: { 'do': 'pasteGrid',  'idgrid': idgrid }
				}).done(function() {
					gridrepository.afterInvalidation();
//					gridrepository.allowGridStoreable();

//					console.log('allow paste');
				});
//				gridrepository.allowGridStoreable();
//				gridrepository.allowGridPaste();
			}
		});
	};

	gridrepository.createItemInRepo = function( id, appendToElement )
	{
		var newblock = $( "<div class='stored-grid'></div>" ).text( "prvek č." + id );
		newblock.attr('data-id', id );
//		newblock.append( "<div>delete</div>" );
//		var links = $( "<a href='?do=deleteFromRepo&idgrid="+id+"'>delete</a>" ).text( "prvek č." + id );
//		links.append( newblock );

		newblock.appendTo( appendToElement );
		var stored = $(appendToElement).find( '.stored-grid' );
//		stored.append("<div>delete</div>");
	}

	gridrepository.allowGridStoreable = function() {
		privateDisableSelection();
		$( ".mydrop" ).droppable({
			activeClass: "mydrop-active",
			hoverClass: "mydrop-hover",
			accept: ".grid-block",
			tolerance: "touch",
			greedy: false,
			drop: function( event, ui ) {
				var id = ui.helper.data('id');
				gridrepository.createItemInRepo( id, this );

				$.nette.ajax({
					data: { 'do': 'copyGrid',  'idgrid': id }
				}).done(function() {
					// gridrepository.afterInvalidation();
					gridrepository.afterStoreToRepo();
				});

				gridrepository.allowGridPaste();

			  }
		});
	};

}) ( AntoninRykalsky.gridrepository = AntoninRykalsky.gridrepository || {});
