(function(placement, undefined){


	placement._placeInner= function( element ){
		$(element).width( $(element).parent('.grid-block').width()-12 );
		$(element).height( $(element).parent('.grid-block').height()-12 );
	};

	placement._recount = function() {
		var gridder = AntoninRykalsky.gridder;
		// checkout values
		gridder.gridWidth = (gridder.width)/gridder.gridCount;
		$('.wrapper-block').width( ((gridder.width/gridder.gridCount)*gridder.maxGrid) );

		if( gridder.log === 1)
		{
			console.log("recounting");
			console.log("gridder width is " + gridder.width);
			console.log("setting gridder gridWidth to " + ((gridder.width/gridder.gridCount)*gridder.maxGrid) );
		}

		_findoutBreaks();
		var recountGridder = gridder;


		$('.wrapper-block').each(function(){
			$(this).children('.grid-block').each(function(){
				// omezeni vykresleni sirky bloku pres max grids
				if( $(this).data('width') > recountGridder.maxGrid )
				{
					$(this).data('width', recountGridder.maxGrid);
				}
				$(this).css('width', recountGridder.gridWidth*$(this).data('width') + "px" );
				$(this).addClass('resizable');
			});
		});
		$( ".resizable" ).resizable({
			grid: [ gridder.gridWidth, 150 ],
			maxWidth: gridder.gridWidth * gridder.gridCount +5,
			minWidth: gridder.gridWidth,
			resize: function( event, ui ) {
				gridder.placement._placeInner( ui.element.find('.inner') );
				var size = Math.round(ui.size.width / gridder.gridWidth);
				if( size > 12 ) ui.size.width = Math.round( gridder.gridWidth * 12 );
				ui.element.attr('data-width',size );
			},
			stop: function( event, ui ) {
				var size = ui.element.attr('data-width');
				$.ajax({url: '?do=resizeGrid',data: {'grid': ui.element.data('id'),'width': size }});
			}
		});
		$('.grid-block .inner').each(function(index, element)
		{
			placement._placeInner( element ) ;
		});
	};

	var _findoutBreaks = function() {
		var actualRowWidth = 0;
		var maxRows = 1;
		var i=0;
		var gridder = AntoninRykalsky.gridder;
		var debug = gridder.debug;
		$('.grid-block').each(function(){
			i=i+1;
			var w = $(this).data('width');
			actualRowWidth+=w;

			// presahuje
			if( actualRowWidth > 12 )
			{
				if( maxRows < i-1 ) maxRows=i-1;
				i=1;
				actualRowWidth = w;

				if( debug )
					$(this).find('.inner').append("presahuje - i"+i+" w"+w+" arw"+actualRowWidth);

				// do dalsiho kola
				i=1;
			}

			// sedi
			else {
				if( debug )
				$(this).find('.inner').append("sedi - i"+i+" w"+w+" arw"+actualRowWidth);
			}

			if( maxRows < i ) maxRows=i;
		});

		return maxRows;
	};

}) ( AntoninRykalsky.gridder.placement = AntoninRykalsky.gridder.placement || {} );
