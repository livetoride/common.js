
(function ( CmsGridOption, undefined ) {

	CmsGridOption.register = function() {
		menuRemoveFrom();
		CmsGridOption.hideLinks();
		settingClickHandler();
		menuAddToMenuHandler();
	};


	CmsGridOption.getContentByGrid = function( grid ) {
		var type = grid.attr('data-type');
		var typeid = grid.attr('data-typeid');
		var NewDialog = '<div dir=rtl id="addToMenuDialog">';

		$(grid).find('.grid-content > a').each(function(idx, element){
			link = element;
			NewDialog +='<a target="_blank" href="'+link+'">'+ $(this).html() +'<br />';
		});

		NewDialog +='</div>';
		return NewDialog;
	};

	CmsGridOption.hideLinks = function() {
		$('.grid-content > a').each(function(idx, element){
			$(this).hide();
		});

		$('.grid-block .grid-inline-edit').hide();
		$('.grid-block .grid-classic-edit').hide();
		$('.grid-block .editace galerie').hide();
	};


	var settingClickHandler = function(  ) {
		$('.settings-control').unbind('click');
		$('.settings-control').click(function() {
			var el = $(this);
			var grid = $(el).parents('.grid-block');
			NewDialog = CmsGridOption.getContentByGrid( grid );
			var wrap = $('<div class="settings-options"></div>');
			$('body').append( wrap.append(NewDialog) );
			$('#addToMenuDialog').dialog({
				modal: true,
				title: CmsGridOption.title,
				dialogClass: "mydialog",
				show: 'clip',
				width: 600,
				hide: 'clip',
				close: function( event, ui ) {
					$('#addToMenuDialog').dialog('destroy').remove();
				}
			});
			
			// close dialog after click to one of links
			$('#addToMenuDialog a').click(function(){
				$('#addToMenuDialog').dialog('destroy').remove();
			});
			
		});
	};

	var siteJsonLink = '';

	CmsGridOption.setJsonLink = function( link )
	{
		siteJsonLink = link;
	};

	var transfortToSelect2 = function() {
		$("#frmsetContext-addContext2").select2({
			placeholder: "Vyhledat položku menu pro zařazení",
			minimumInputLength: 0,
			id: function(results){ return results.id; },
			ajax: {
				url: siteJsonLink,
				dataType: 'json',
				quietMillis: 100,
				data: function (term, page) { // page is the one-based page number tracked by Select2
					return {
						q: term, //search term
						page_limit: 10, // page size
						page: page // page number
						//apikey: "ju6z9mjyajq2djue3gbvv26t" // please do not use so this example keeps working
					};
				},
				results: function (data, page) {
					var more = (page * 10) < data.total; // whether or not there are more results available

					// notice we return the value of more so Select2 knows if more results can be loaded
					return { results: data.results, more: more};
				}
			},
			initSelection: function(element, callback) {
				var id=$(element).val();
				if (id!=="") {
					$.ajax(siteJsonLink, {
						data: {
							default: 1
						},
						dataType: "json"
					}).done(function(data) {
						callback( data.results[0] );
					});
				}
			},
			escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
		});
	};

	var addToMenuClickHandler = function() {
		var NewDialog = '<div dir=rtl id="addToMenuDialog">\n\
			<input type="hidden" id="frmsetContext-addContext2" /><br />\n\
			<input type="submit" name="addContextBtn" value="Přidat do" id="addContextBtn" />\n\
		</div>';

		
		
		$('body').append(NewDialog);
		$('#addToMenuDialog').dialog({
			modal: true,
			title: "Zařazení do menu",
			dialogClass: "mydialog",
			show: 'clip',
			width: 600,
			hide: 'clip',
			close: function( event, ui ) {
				$('#addToMenuDialog').dialog('destroy').remove();
				$('#addToMenuDialog').remove();
			}
		});
	}

	var menuRemoveFrom = function() {
		$('.cms-remove-from-menu').unbind('click');
		$(".cms-remove-from-menu").click(function(){
			var really = confirm('Opravdu odstranit z zařazení?');
			if( !really ) {
				return false;
			}

			var href = $(this).attr('data-href');
			$.nette.ajax({
				url: href
			}).done(function() {
				AntoninRykalsky.gridrepository.afterInvalidation();
			});
		});
	};

	var menuAddToMenuHandler = function() {
		$('#addToMenu').unbind('click');
		$('#addToMenu').click(function(){
			addToMenuClickHandler();
			transfortToSelect2();
			
			

			$('#addContextBtn').click(function(){
					var i = $('#frmsetContext-addContext2').val();
					$.nette.ajax({
						data: { 'do': 'addToMenu', 'parentid': i }
					}).done(function() {
						AntoninRykalsky.gridrepository.afterInvalidation();
						$('#addToMenuDialog').dialog('destroy').remove();
					});
				return false;
			});

			return false;
		});
	};


}) ( AntoninRykalsky.CmsGridOption = AntoninRykalsky.CmsGridOption || {});
