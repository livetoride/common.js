var AntoninRykalsky = AntoninRykalsky || {};


$(document).ready(function(){
	AntoninRykalsky.addContent.register();
	AntoninRykalsky.gridder.register();
	AntoninRykalsky.CmsGridOption.register();
	AntoninRykalsky.gridrepository.allowGridPaste();
	AntoninRykalsky.gridrepository.afterInvalidation();
});
$(window).resize(function(){
	// AntoninRykalsky.addContent.renderer.resize();
});


(function ( gridder, undefined ) {
	gridder.gridCount= 12;
	gridder.maxGrid= 12;
	gridder.width= 920;
	gridder.gridWidth= 0;
	gridder.log= 0;
	gridder._order= new Array();

	gridder.setMaxGrid = function( maxGrid ) {
		gridder.maxGrid = maxGrid;

	};

	gridder.registerDeleteButton = function() {
		$('.remove-wrapper').click(function(){
			var id = $(this).parents('.grid-block').data('id');
			console.log("opravdu smazat");
			if (confirm("Opravdu smazat?")) {


				$(this).parents('.grid-block').hide();
				$.nette.ajax({
					data: { 'do': 'removeGrid', 'gridid': id }
				}).done(function() {
					AntoninRykalsky.gridrepository.afterInvalidation();
				});
			}
		});
	};
	
	gridder.register = function() {
		if(gridder.log === 1)
		{
			console.log("Registering gridder class");
		}
		gridder.placement._recount();

		
		$( ".grid-sortable" ).hover(function() {
			document.onselectstart = function () { return false; };
		  }, function(){
			  document.onselectstart = null;
		  });
		$( ".grid-sortable" ).sortable({
			helper: "clone",
			scroll: false,
			appendTo: 'body',
			start: function( event, ui ) {
				document.onselectstart = function () { return false; };
				gridder.order = new Array();

				$(this ).find('.grid-block:not(.ui-sortable-placeholder)').each(function(){
					var id = $(this).data('id');
					if( typeof id !== "undefined" )
					{
						gridder.order.push( id );
					}
				});

			},
			stop: function( event, ui ) {
				document.onselectstart = null;
				var order = new Array();
				$('.grid-block').each(function(){
					var id = $(this).data('id');
					if( typeof id !== "undefined" )
					{
						order.push( id );
					}
				});
				function arrays_equal(a,b) { return !(a<b || b<a); }
				console.log(gridder.order);
				console.log(order);
				if( !arrays_equal( gridder.order, order)) {
					$.nette.ajax({
						data: {'do': 'changeOrder', 'order': order.join('-') }
					}).done(function() {
						AntoninRykalsky.gridrepository.afterInvalidation();
					});
				}
			}
		});
	};

}) ( AntoninRykalsky.gridder = AntoninRykalsky.gridder || {});
