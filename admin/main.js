/*
$(document).ready(function(){
	// inicializace uniformjs
	$(".datagrid input").addClass('skip_these');
	$(".datagrid select").addClass('skip_these');
	$("#frm-activeSeason select").addClass('skip_these');
	$("#frm-activeSeason input").addClass('skip_these');
	$('input').not(".skip_these").uniform();
	$('select').not(".skip_these").css('width', 'auto');
	$('select').not(".skip_these").uniform();
	$('textarea').uniform();
	$('a.button').uniform();

	// in datagrid except
	$('#frm-settings input').uniform();

});
*/

var adminModal = {
	open : function( id ) {
		 var w = $( id ).width();
		 $( id ).dialog({
			width: w,
			modal: true
		 });
		 // var widget = $( id ).dialog( "widget" );
		 // widget.find('.ui-dialog-titlebar').hide();
		 $( id ).on( "dialogbeforeclose", function( event, ui ) {
			 $( id ).dialog( "destroy" );
			 $( id ).remove();
		 } );
		 $('.ui-widget-overlay').remove();
	}
};
